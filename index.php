<?php
$status = true;
include('creds.php');
$date = date('d/m/Y_H:i:s');
// Connexion
try {
$connexion = new PDO('mysql:host='.$host.';dbname='.$db.'; charset=utf8', $user, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
} catch (PDOExeption $e) {
    echo 'Error';
}

function getNumURL($shorten) {
    global $connexion;
    $select = $connexion->prepare('SELECT COUNT(*) FROM urls WHERE shorturl=:shorturl');
    $select->execute(array('shorturl' => $shorten));

    $count = $select->fetch();
    return $count[0];
}


function generate_random_letters($length) {
    $random = '';
    for ($i = 0; $i < $length; $i++) {
        $random .= rand(0, 1) ? rand(0, 9) : chr(rand(ord('a'), ord('z')));
    }
    return $random;
}

if($status == true) {
    if(isset($_POST['sendURL']) AND !empty($_POST['url'])) {
        if(preg_match_all("#http(s)?://(www.)?(.)+#", $_POST['url'], $result)) {
            do {
                $shorturl = generate_random_letters(8);
            } while (getNumURL($shorturl) > 0);
            $longurl = $_POST['url'];
            $addToBdd = $connexion->prepare('INSERT INTO urls (longurl, shorturl, date) VALUES (:longurl, :shorturl, :date)');
            $addToBdd->execute(array(
                ':longurl' => $longurl,
                ':shorturl' => $shorturl,
                ':date' => $date
            ));

            header('Location: index.php?link='.$shorturl);
            exit();
        } else {
            header('Location: index.php?err=syntax');
            exit();
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="css.css">
    <link rel="stylesheet" href="//cdn.krafting.net/fonts/jura.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>KRAFTING.net - YAUSS (Yet Another URL Shortener Service)</title>
</head>
<body>
    <header>
        <h1><a href="https://url.krafting.net">KRAFTING.net</a></h1>
        <h2><a href="./">YAUSS - Yet Another URL Shortener Service</a></h2>
    </header>
    <div class="underglow"></div>
    <div class="content">
        <div class="yauss">
            <p>YAUSS is a free service offered by Krafting. It allows you to simply shorten an URL to share it.</p>
            <small>Allowed URL format : http(s)?://(www.)?(.)+ </small>
            <?php if(isset($_GET['link'])) { echo '<label class="success">URL shortened! Your link: <a href="'.$urlSite.$_GET['link'].'">'.$urlSite.$_GET['link'].'</a><a href="#qrcode" class="qrlink">[QR Code]</a></label>'; }?>
            <?php if(isset($_GET['err'])) { echo '<label class="error">URL syntax is not correct. Accepted format: <b> http(s)?://(www.)?(.)+ </b></label>'; }?>
            <?php if(isset($_GET['urlnotfound'])) { echo '<label class="error">This URL is not in the database. Sorry :(</label>'; }?>
            <?php if(!$status) { echo '<label class="error">YAUSS has been disabled by Krafting. You cannot use this service for the moment.</label>'; }?>

            <form method="POST">
                <input placeholder="Your link goes here. e.g. https://odysee.com/@Krafting:2" type="text" name="url" pattern="http(s)?://(www.)?(.)+">
                <input type="submit" name="sendURL" value="Shorten!">
            </form>
            <div id="qrcode"></div>
        </div>
    </div>
    
    <div class="underglow"></div>
    <footer>
        <a href="https://www.krafting.net/contact.php">Contact</a> 2021 - 2022 - url.krafting.net <a href="https://www.krafting.net/terms.php" title="Terms & Privacy">Terms & Privacy</a>
	</footer>
    <script src="//cdn.krafting.net/js/qrcode.min.js"></script>
    <script src="js.js"></script>

</body>
</html>