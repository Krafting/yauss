CREATE TABLE `urls` (
 `idurl` int(11) NOT NULL AUTO_INCREMENT,
 `longurl` text NOT NULL,
 `shorturl` text NOT NULL,
 `date` text NOT NULL,
 PRIMARY KEY (`idurl`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1
