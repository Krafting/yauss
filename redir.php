<?php
include('creds.php');

// Connexion
try {
    $connexion = new PDO('mysql:host='.$host.';dbname='.$db.'; charset=utf8', $user, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
} catch (PDOExeption $e) {
    echo 'Error';
}

# Regex pas utile, elle est aussi dans le HTACCESS, mais bon, on sais jamais!
if(preg_match_all("#[A-z0-9]+#", $_GET['link'], $result)) {
    $select = $connexion->prepare('SELECT COUNT(*), longurl FROM urls WHERE shorturl=:shorturl');
    $select->execute(array('shorturl' => $_GET['link']));
    $infos = $select->fetch();

    if($infos[0] == 1) {
        header('Location: '.$infos['longurl']);
        exit();
    } else {
        header('Location: index.php?urlnotfound=url');
        exit();
    }

} else {
    header('Location: index.php?notfound=shortened');
    exit();
}


?>

<?php if(isset($_GET['link'])) { echo "SUCCESS".$_GET['link']; }?>