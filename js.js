function getUrlParam(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results == null) { return null; } else { 
        return results[1]; 
    }
};
param = getUrlParam('link');
if(param) {
    document.getElementById('qrcode').style.display = "block";
    var text = "https://url.krafting.net/" + param;
    var qrcode = new QRCode("qrcode");
    qrcode.makeCode(text);
} 